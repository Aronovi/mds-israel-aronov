package com.example;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.PriorityBlockingQueue;
import javafx.util.Pair;

public class MyDataStructure {

	final int capacity;
	 Map<String, Pair <Object, Integer>> SyncTripleLHMap;
	 static PriorityBlockingQueue<LivingItem> SyncPrioriyQueueByTTL;
	 Thread [] runningItemsThreads;
	
	public MyDataStructure(int capacity) {
		this.capacity=capacity;
		 SyncPrioriyQueueByTTL=new PriorityBlockingQueue<LivingItem>(); 
		 Map<String, Pair <Object, Integer>> TripleLHMap=new LinkedHashMap<String, Pair<Object, Integer>>(capacity, 1, false);
		 SyncTripleLHMap=Collections.synchronizedMap(TripleLHMap);
		 runningItemsThreads=new Thread [5];
		 
		 // activate 5 threads for running all the positive TTL items
		for(int i=0; i<5; i++) 
		{
			runningItemsThreads[i] =runTheLivingItems();
			runningItemsThreads[i].start();
		}
	}
	
	/**
	 * create new thread that need to run on all over the items with positive timeToLive and then delete them
	 */
	private Thread runTheLivingItems(){
		Thread runningItemsThread = new Thread(){
		    public void run(){
		    	while(true)
		    	{	
		    	  if(!SyncPrioriyQueueByTTL.isEmpty())
		    		{
		    		 LivingItem  item=SyncPrioriyQueueByTTL.poll();	
		             if(item!=null && get(item.getKey())!=null) //for the case when 2 or more threads pass the if condition but there is less items in queue then threads in this phase 
		    	      try {
		    	    	   this.setName(item.getKey()); //In order to identify which thread works on which item
					       Thread.sleep(Integer.valueOf(item.getTimeToLive()));
					       this.setName("done");
					       remove(item.getKey());
				           } 
		    	      catch (InterruptedException e) {}
				}}
		    }};
		  return runningItemsThread;
	}
	
	/**
	 * add value to the data structure. complexity is o(1)
	 * @param key
	 * @param value
	 * @param timeToLive
	 */
	public void put(String key, Object value, int timeToLive) {
		if(size()==capacity)
		{
			 Entry<String, Pair<Object, Integer>> entry = SyncTripleLHMap.entrySet().iterator().next();
			 String keyToDelete = entry.getKey();
			 remove(keyToDelete);
		} 
		
		if(timeToLive>=0)
		{
			Pair<Object, Integer> newItem=new Pair<Object, Integer>(value, timeToLive);
			SyncTripleLHMap.put(key, newItem);
			if(timeToLive>0)
			synchronized(SyncPrioriyQueueByTTL)
			{ //the adding thread is running in the background, therefore its doesn't effect the time complexity
			Thread addingThread=addingItemToQueue(key, timeToLive);
			addingThread.start();
			}
		}
		// I'm not allow to put item with negative timeToLive 
	}
	
	/**
	 * creating a new thread that need to add the new item to the priority queue
	 * @param key
	 * @param timeToLive
	 */
	private Thread addingItemToQueue(String key, int timeToLive)
	{
		Thread addingThread = new Thread(){
		    public void run(){
		    	 SyncPrioriyQueueByTTL.add(new LivingItem(key, timeToLive));
		    }};
		    return addingThread;
	}
	
	/**
	 * remove value from the data structure. complexity is o(1)
	 * @param key
	 */
	public void remove(String key) {
		SyncTripleLHMap.remove(key);
		synchronized(SyncPrioriyQueueByTTL)
		{ //the removing thread is running in the background, therefore its doesn't effect the time complexity
		Thread removingThread=removingItemFromQueue(key);
		removingThread.start();
		}
	}
	
	/**
	 * creating new thread that removing the key item from queue and stop the thread that runs this key item
	 * @param key
	 */
	private Thread removingItemFromQueue(String key)
	{
		Thread removingThread = new Thread(){
		    public void run(){
		    	SyncPrioriyQueueByTTL.remove(get(key));
		    	for(Thread th: runningItemsThreads)
		    	{
		    		if(th.getName().equals(key))
		    		{
		    			th.interrupt();
		    		    th=runTheLivingItems();
		    		    th.start();
		    		    break;
		    		}
		    	}
		    }};
		    return removingThread;
	}

	/**
	 * get value from the data structure. complexity is o(1)
	 * @param key
	 * @return
	 */
	public Object get(String key) {
		if(SyncTripleLHMap.containsKey(key))
		    return SyncTripleLHMap.get(key).getKey();
		else
			return null;
	}

	/**
	 * get number of keys in the data structure. complexity is o(1)
	 * @return number of keys in the data structure
	 */
	public int size() {
		return SyncTripleLHMap.size();
	}
}
