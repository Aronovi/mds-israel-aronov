package com.example;

public class LivingItem implements Comparable <LivingItem> {

   private String key;
   private int timeToLive;
   
   public  LivingItem(String key, int timeToLive) {
	   this.key=key;
	   this.timeToLive=timeToLive;
   }
   
   public String getKey(){
	   return this.key;
   }
   
   public int getTimeToLive(){
	   return this.timeToLive;
   }
   

@Override
public int compareTo(LivingItem other) {
	if(this.timeToLive>other.getTimeToLive())
		return 1;
	else if(this.timeToLive<other.getTimeToLive())
		return -1;
	else
		return 0;
	}
}
